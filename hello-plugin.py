from gimpfu import *
def hello_world():
    pdb.gimp_message('Hello World !....')
register(
	'python-fu-hello-world',
	'Hello World',
	'This is a Hello World Plugin',
	'Muddy Games', 'Muddy Games', '2016',
	'Hello World',
	'*',
	[],
	[],
	hello_world,
	menu='<Image>/Filters/Render')
main()