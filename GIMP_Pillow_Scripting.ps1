<#
RUN THIS SCRIPT AS ADMINISTRATOR
#>

<#
function DownloadFile($url, $targetFile)
https://blogs.msdn.microsoft.com/jasonn/2008/06/13/downloading-files-from-the-internet-in-powershell-with-progress/
#>

function DownloadFile($url, $targetFile)
{

   $uri = New-Object "System.Uri" "$url"
   $request = [System.Net.HttpWebRequest]::Create($uri)
   $request.set_Timeout(15000) #15 second timeout
   $response = $request.GetResponse()
   $totalLength = [System.Math]::Floor($response.get_ContentLength()/1024)
   $responseStream = $response.GetResponseStream()
   $targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $targetFile, Create
   $buffer = new-object byte[] 10KB
   $count = $responseStream.Read($buffer,0,$buffer.length)
   $downloadedBytes = $count

   while ($count -gt 0)
   {

       $targetStream.Write($buffer, 0, $count)
       $count = $responseStream.Read($buffer,0,$buffer.length)
       $downloadedBytes = $downloadedBytes + $count
       Write-Progress -activity "Downloading file '$($url.split('/') | Select -Last 1)'" -status "Downloaded ($([System.Math]::Floor($downloadedBytes/1024))K of $($totalLength)K): " -PercentComplete ((([System.Math]::Floor($downloadedBytes/1024)) / $totalLength)  * 100)
   }

   Write-Progress -activity "Finished downloading file '$($url.split('/') | Select -Last 1)'"

   $targetStream.Flush()
   $targetStream.Close()
   $targetStream.Dispose()
   $responseStream.Dispose()
}

<#
Generates Hello World Python Plugin
#>

function GeneratePlugin($url, $targetFile)
{
    $start_time = Get-Date
    
    Invoke-WebRequest -Uri $url -OutFile $targetFile

    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

    $result = Test-Path "C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\"

	if($result)
	{
	    Copy-Item -Path $targetFile -Destination "C:\Program Files\GIMP 2\lib\gimp\2.0\plug-ins\hello-plugin.py"
	}
	else
	{
	    "Error Directory does not exist"
	}
}

<#
Downloads and Install GIMP
#>

function InstallGimp($url, $targetFile)
{
    $start_time = Get-Date
    downloadFile $url $targetFile
    
    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
    
    $start_time = Get-Date
    
    #Fix Start-Process
    $arguments = "/silent ", "/norestart ", "/SUPPRESSMSGBOXES "
    
    Start-Process $targetFile $arguments -Wait 

    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
}

<#
Installs pip
#>

function InstallPip($url, $targetFile)
{
    downloadFile $url $targetFile
}

<#
Installs Pillow
#>

function InstallPillow()
{
    python -m pip install pillow

    $testFile = "$PSScriptRoot\test.py"

    Out-File -filepath $testFile -Encoding ASCII

    Add-Content $testFile "help('modules PIL')"

    Try
    {
        python $testFile
    }
    Catch
    {
        "Error installing Pillow"
    }

}

#Download URL and Target File
$url = "https://download.gimp.org/mirror/pub/gimp/v2.8/windows/gimp-2.8.18-setup.exe"
$targetFile = "$PSScriptRoot\gimp-setup.exe"

InstallGimp $url $targetFile

$url = "https://bootstrap.pypa.io/get-pip.py"
$targetFile = "$PSScriptRoot\get-pip.py"
InstallPip $url $targetFile
$env:Path = "C:\Program Files\GIMP 2\Python\"

    Try
    {
        python.exe $targetFile
    }
    Catch
    {
        "Error installing PIP"
    }

InstallPillow


$url = "https://bitbucket.org/MuddyGames/gimp-pillow/raw/master/hello-plugin.py"
$targetFile = "$PSScriptRoot\hello-plugin.py"
GeneratePlugin $url $targetFile


# GIMP Command line options, when GIMP loads goto FILE | NEW and then Filters | Render | Hello World
# Hello World message should be displayed on console

$application = "C:\Program Files\GIMP 2\bin\gimp-2.8.exe"

#Starting GIMP   
& $application --no-data --verbose --no-splash --console-messages